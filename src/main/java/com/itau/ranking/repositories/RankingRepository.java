package com.itau.ranking.repositories;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.ranking.models.RankingModel;

public interface RankingRepository extends CrudRepository<RankingModel, Long>{
	
	ArrayList<RankingModel> findTop10ByOrderByPontuacaoDesc();

	Optional<RankingModel> findByNomeJogoAndUsernameJogador(String nomeJogo, String usernameJogador);

	ArrayList<RankingModel> findTop10ByNomeJogoOrderByPontuacaoDesc(String nomeJogo);

}
