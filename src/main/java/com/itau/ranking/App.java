package com.itau.ranking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import com.itau.ranking.logs.LogReader;

@SpringBootApplication
@EnableJms
public class App 
{
//	@Bean
//	public JmsListenerContainerFactory<?> pubsub(ConnectionFactory connectionFactory,
//	                                                DefaultJmsListenerContainerFactoryConfigurer configurer) {
//	    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
//	    configurer.configure(factory, connectionFactory);
//	    factory.setPubSubDomain(true);
//	    return factory;
//	}

    public static void main( String[] args )
    {

        SpringApplication.run(App.class, args);
//        LogReader.criarThreads();
    }
}
