package com.itau.ranking.controllers;

import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ranking.models.RankingModel;
import com.itau.ranking.repositories.RankingRepository;

@RestController
public class RankingController {
	
	@Autowired
	RankingRepository rankingRepository;
	
	@RequestMapping(method=RequestMethod.POST, path="/gravarPontuacao")
	public ResponseEntity<?> gravarNovoRanking(@Valid @RequestBody RankingModel ranking) {
		
		Optional<RankingModel> optionalRankingModel = rankingRepository.findByNomeJogoAndUsernameJogador(ranking.getNomeJogo(), ranking.getUsernameJogador());
		
		if(optionalRankingModel.isPresent()) {
			if(optionalRankingModel.get().getPontuacao() < ranking.getPontuacao()) {
				optionalRankingModel.get().setPontuacao(ranking.getPontuacao());
				rankingRepository.save(optionalRankingModel.get());
				return ResponseEntity.ok().build();
			}
			else {
				return ResponseEntity.status(202).body(optionalRankingModel);
			}
		}
		
		return ResponseEntity.ok().body(rankingRepository.save(ranking));
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/geralRanking")
	public ResponseEntity<ArrayList<RankingModel>> buscarRankingGeral() {
		return ResponseEntity.ok().body(rankingRepository.findTop10ByOrderByPontuacaoDesc());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/ranking/{nomeJogo}")
	public ResponseEntity<?> buscarRankingJogo(@PathVariable String nomeJogo) {
		ArrayList<RankingModel> listaRankingModel = rankingRepository.findTop10ByNomeJogoOrderByPontuacaoDesc(nomeJogo);
		
		if(listaRankingModel.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(listaRankingModel);
	}

}
