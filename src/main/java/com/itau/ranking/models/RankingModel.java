package com.itau.ranking.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class RankingModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String nomeJogo;
	
	@NotNull
	private String usernameJogador;
	
	@Min(0)
	private int pontuacao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public String getUsernameJogador() {
		return usernameJogador;
	}

	public void setUsernameJogador(String usernameJogador) {
		this.usernameJogador = usernameJogador;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	@Override
	public String toString() {
		return "[nomeJogo=" + nomeJogo + ", usernameJogador=" + usernameJogador + ", pontuacao="
				+ pontuacao + "]";
	}
	
	

}
