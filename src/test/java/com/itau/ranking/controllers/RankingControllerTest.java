package com.itau.ranking.controllers;

import java.util.ArrayList;
import java.util.Optional;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.ranking.models.RankingModel;
import com.itau.ranking.repositories.RankingRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(RankingController.class)
public class RankingControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	RankingRepository rankingRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void testarInserirPontuacao() throws Exception {
		RankingModel ranking = new RankingModel();
		
		ranking.setNomeJogo("Teste1");
		ranking.setPontuacao(10);
		ranking.setUsernameJogador("Lala");
		
		Optional<RankingModel> optionalRankingModel = Optional.empty();
		
		when(rankingRepository.findByNomeJogoAndUsernameJogador(any(String.class),any(String.class))).thenReturn(optionalRankingModel);
		
		String json = mapper.writeValueAsString(ranking);
		
		mockMvc.perform(post("/gravarPontuacao")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect( status().is(200) );
		
	}
	
	@Test
	public void testarAtualizarPontuacao() throws Exception {
		RankingModel ranking = new RankingModel();
		RankingModel rankingOld = new RankingModel();
		
		ranking.setNomeJogo("Teste2");
		ranking.setPontuacao(11);
		ranking.setUsernameJogador("Daniel");
		
		rankingOld.setNomeJogo("Teste2");
		rankingOld.setPontuacao(9);
		rankingOld.setUsernameJogador("Daniel");
		
		Optional<RankingModel> optionalRankingModel = Optional.of(rankingOld);
		
		when(rankingRepository.findByNomeJogoAndUsernameJogador(any(String.class),any(String.class))).thenReturn(optionalRankingModel);
		
		if(rankingOld.getPontuacao() < ranking.getPontuacao()) {
			
			String json = mapper.writeValueAsString(ranking);
			
			mockMvc.perform(post("/gravarPontuacao")
					.content(json)
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect( status().is(200) );
		}
		
	}
	
	@Test
	public void testarNaoAtualizarPontuacao() throws Exception {
		RankingModel ranking = new RankingModel();
		RankingModel rankingOld = new RankingModel();
		
		ranking.setNomeJogo("Teste3");
		ranking.setPontuacao(11);
		ranking.setUsernameJogador("Roberta");
		
		rankingOld.setNomeJogo("Teste3");
		rankingOld.setPontuacao(12);
		rankingOld.setUsernameJogador("Roberta");
		
		Optional<RankingModel> optionalRankingModel = Optional.of(rankingOld);
		
		when(rankingRepository.findByNomeJogoAndUsernameJogador(any(String.class),any(String.class))).thenReturn(optionalRankingModel);
		
		if(rankingOld.getPontuacao() > ranking.getPontuacao()) {
			
			String json = mapper.writeValueAsString(rankingOld);
			
			mockMvc.perform(post("/gravarPontuacao")
					.content(json)
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect( status().is(202) )
			.andExpect(jsonPath("$.nomeJogo", equalTo("Teste3")))
			.andExpect(jsonPath("$.usernameJogador", equalTo("Roberta")))
			.andExpect(jsonPath("$.pontuacao", equalTo(12)));
		}
	}
	
	@Test
	public void testarDevolverRankingGeral() throws Exception {
		ArrayList<RankingModel> arrayRanking = new ArrayList<>();
		RankingModel ranking = new RankingModel();
		
		ranking.setNomeJogo("Teste1");
		ranking.setPontuacao(10);
		ranking.setUsernameJogador("Lala");
		arrayRanking.add(0,ranking);
		
		ranking.setNomeJogo("Teste2");
		ranking.setPontuacao(11);
		ranking.setUsernameJogador("Daniel");
		arrayRanking.add(1,ranking);
		
		ranking.setNomeJogo("Teste3");
		ranking.setPontuacao(12);
		ranking.setUsernameJogador("Roberta");
		arrayRanking.add(2,ranking);
		
		when(rankingRepository.findTop10ByOrderByPontuacaoDesc()).thenReturn(arrayRanking);
		
		String json = mapper.writeValueAsString(arrayRanking);
		
		MvcResult result = mockMvc.perform(get("/geralRanking")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andReturn();
		
		String jsonResult = result.getResponse().getContentAsString();
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(arrayRanking);
		
		JSONAssert.assertEquals(jsonResult, jsonExpected, false);
		
	}
	
	@Test
	public void testarDevolverRankingEspecifico() throws Exception {
		ArrayList<RankingModel> arrayRanking = new ArrayList<>();
		RankingModel ranking = new RankingModel();
		
		ranking.setNomeJogo("Quiz");
		ranking.setPontuacao(10);
		ranking.setUsernameJogador("Lala");
		arrayRanking.add(0,ranking);
		
		ranking.setNomeJogo("Quiz");
		ranking.setPontuacao(11);
		ranking.setUsernameJogador("Daniel");
		arrayRanking.add(1,ranking);
		
		ranking.setNomeJogo("Quiz");
		ranking.setPontuacao(12);
		ranking.setUsernameJogador("Roberta");
		arrayRanking.add(2,ranking);
		
		when(rankingRepository.findTop10ByNomeJogoOrderByPontuacaoDesc(any(String.class))).thenReturn(arrayRanking);
		
		String json = mapper.writeValueAsString(arrayRanking);
		
		MvcResult result = mockMvc.perform(get("/ranking/Quiz")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andReturn();
		
		String jsonResult = result.getResponse().getContentAsString();
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonExpected = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(arrayRanking);
		
		JSONAssert.assertEquals(jsonResult, jsonExpected, false);
		
	}
	
	@Test
	public void testarRankingEspecificoVazio() throws Exception {
		ArrayList<RankingModel> arrayRanking = new ArrayList<>();
		
		when(rankingRepository.findTop10ByNomeJogoOrderByPontuacaoDesc(any(String.class))).thenReturn(arrayRanking);
		
		String json = mapper.writeValueAsString(arrayRanking);

		mockMvc.perform(get("/ranking/Quiz")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect( status().is(404) );
		
	}
	
}
