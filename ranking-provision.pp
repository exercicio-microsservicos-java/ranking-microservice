
package {'java-1.8.0-openjdk':
  ensure => latest,
}

package {'mariadb':
  ensure => latest,
}

package {'mariadb-server':
  ensure => latest,
}

package {'maven':
  ensure => latest,
}

service {'mariadb':
  ensure   => running,
  enable   => true,
}

exec {'mariadb configure permissions':
  command => '/usr/bin/chmod 755 /tmp/mysql-configure.sh'
}

exec {'mariadb configure execute':
  command => '/tmp/mysql-configure.sh'
}

exec {'criar diretorio':
  command => '/usr/bin/mkdir -p /var/log/nohup'
}

exec {'project install':
  command => '/usr/bin/nohup /usr/bin/java -jar Ranking.jar > /var/log/nohup/ranking.log &'
}

